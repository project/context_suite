<?php

namespace Drupal\context_suite\EventSubscriber;

use Drupal\context\ContextManager;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class Subscriber.
 *
 * @package Drupal\context_suite\EventSubscriber
 */
class Subscriber implements EventSubscriberInterface {

  /**
   * Variable check is executed.
   *
   * @var bool
   */
  protected $isExecuted;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The context manager.
   *
   * @var \Drupal\context\ContextManager
   */
  protected $contextManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The logger chanel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Set route match service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The  route match service.
   */
  public function setRouteMatch(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * Context manager.
   *
   * @param \Drupal\context\ContextManager $context_manager
   *   The context manager.
   */
  public function setContextManager(ContextManager $context_manager) {
    $this->contextManager = $context_manager;
  }

  /**
   * Set language manager.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function setLanguageManager(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * Set logger factory.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $channel_factory
   *   The logger factory.
   */
  public function setLogger(LoggerChannelFactory $channel_factory) {
    $this->logger = $channel_factory->get('context_suite');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST] = ['onRequest', -100];
    return $events;
  }

  /**
   * Handle request.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The GetResponseEvent.
   */
  public function onRequest(GetResponseEvent $event) {
    if (!$event->isMasterRequest()) {
      return;
    }
    if ($this->isExecuted) {
      return;
    }
    $this->isExecuted = TRUE;
    foreach ($this->contextManager->getActiveReactions('redirect_route') as $reaction) {
      $data = $reaction->execute();
      $language = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT);
      if ($data['route_type'] === 'route') {
        try {
          $redirect = new TrustedRedirectResponse(Url::fromRoute($data['route_name'], ['language' => $language])
            ->toString());
          $event->setResponse($redirect);
        }
        catch (\Exception $exception) {
          $this->logger->error($exception->getMessage());
        }
      }

      if ($data['route_type'] === 'url') {
        try {
          $redirect = new TrustedRedirectResponse(Url::fromUserInput($data['route_url'], ['language' => $language])
            ->toString());
          $event->setResponse($redirect);
        }
        catch (\Exception $exception) {
          $this->logger->error($exception->getMessage());
        }
      }

      if ($data['route_type'] == '404') {
        throw new NotFoundHttpException();
      }
      if ($data['route_type'] == '403') {
        throw new AccessDeniedHttpException();
      }
      break;
    }
  }

}

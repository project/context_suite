<?php

namespace Drupal\context_suite\Plugin\ContextReaction;

use Drupal\context\ContextReactionPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a content reaction that adds a new css class.
 *
 * @ContextReaction(
 *   id = "redirect_route",
 *   label = @Translation("Redirect to route")
 * )
 */
class RedirectRoute extends ContextReactionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'route_name' => '',
      'route_type' => 'front',
      'route_url' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->getConfiguration()['route_name'];
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['route_type'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => $this->t('Redirect'),
      '#default_value' => $this->getConfiguration()['route_type'],
      '#options' => [
        'front' => $this->t('Front page'),
        '404' => $this->t('404: Page not found'),
        '403' => $this->t('403: Forbidden'),
        'route' => $this->t('Route'),
        'url' => $this->t('Url'),
      ],
    ];

    $form['route_name'] = [
      '#title' => $this->t('Route name'),
      '#type' => 'textfield',
      '#description' => $this->t('Route name to redirect.'),
      '#default_value' => $this->getConfiguration()['route_name'],
      '#states' => [
        'visible' => [
          ':input[name="reactions[redirect_route][route_type]"]' => ['value' => 'route'],
        ],
      ],
    ];

    $form['route_url'] = [
      '#title' => $this->t('Route url'),
      '#type' => 'textfield',
      '#description' => $this->t('Url to redirect.'),
      '#default_value' => $this->getConfiguration()['route_url'],
      '#states' => [
        'visible' => [
          ':input[name="reactions[redirect_route][route_type]"]' => ['value' => 'url'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration([
      'route_name' => $form_state->getValue('route_name'),
      'route_type' => $form_state->getValue('route_type'),
      'route_url' => $form_state->getValue('route_url'),
    ]);
  }

}

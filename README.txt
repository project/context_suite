CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

Context suite collection context plugin:
    - Request Path Regex: Use Path regex with condition
    - Redirect to route: Reaction for redirect to front-page,
                         404 page, 403 page and custom route.


REQUIREMENTS
------------

This module requires context module.

INSTALLATION
------------

Install the module as you would normally install a contributed Drupal
module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
--------------

Configuration:

Navigate to: Home > Administration > Structure > Context
